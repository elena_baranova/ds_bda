/**
 * 
 */
package hw1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;

import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.io.LongWritable;
import org.junit.Before;
import org.junit.Test;

/**
 * @author cloudera
 *
 */
public class UsersCountTest {

    /*
    * Declare harnesses that let you test a mapper, a reducer, and a mapper and
    * a reducer working together.
    */
    MapDriver<LongWritable, Text, AccessLogWritable, IntWritable> mapDriver;
    ReduceDriver<AccessLogWritable,IntWritable,Text,IntWritable> reduceDriver;
    MapReduceDriver<LongWritable, Text, AccessLogWritable, IntWritable, Text, IntWritable> mapReduceDriver;
    
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		/*
		* Set up the mapper test harness.
		*/
		UsersCount.UsersCounterMapper mapper = new UsersCount.UsersCounterMapper();
		mapDriver = new MapDriver();
		mapDriver.setMapper(mapper);

		/*
		* Set up the reducer test harness.
		*/
		UsersCount.IntSumReducer reducer = new UsersCount.IntSumReducer();
		reduceDriver = new ReduceDriver ();
		reduceDriver.setReducer(reducer);

		/*
		* Set up the mapper/reducer test harness.
		*/
		mapReduceDriver = new MapReduceDriver();
	 	mapReduceDriver.setMapper(mapper);
		mapReduceDriver.setReducer(reducer);

	}

    /* 
    * Test the mapper.
    */
    
    @Test
    public void testMapper() throws IOException {

        String inputString = new String(Files.readAllBytes(Paths.get("access")));
        AccessLogWritable UagentFromLog1 = new AccessLogWritable();
        
        Text BrwName1 = new Text("Chrome 54");
        Text BrwVersion1 = new Text("54.0.2840.59"); 
        Text TypeOfBrw1 = new Text("WEB_BROWSER");
        
        UagentFromLog1.set(BrwName1, BrwVersion1, TypeOfBrw1);
        
        mapDriver.withInput(new LongWritable(), new Text(inputString));

        mapDriver.addOutput(UagentFromLog1, new IntWritable(1));
        mapDriver.runTest();
    }
    
    /*
    * Test the reducer.
    */
    @Test
    public void testReducer() throws IOException {

        List<IntWritable> values = new ArrayList();
        values.add(new IntWritable(1));
        values.add(new IntWritable(1));
        
        Text BrwName = new Text("Firefox");
        Text BrwVersion = new Text("49.0"); 
        Text TypeOfBrw = new Text("WEB_BROWSER");

        reduceDriver.withInput(new AccessLogWritable (BrwName, BrwVersion, TypeOfBrw), values);
        reduceDriver.withOutput(new Text("Browser [Name=Firefox, version=49.0, type=WEB_BROWSER]"), new IntWritable(2));
        reduceDriver.runTest();
    }

    /*
    * Test the mapper and reducer working together.
    */
    @Test
    public void testMapReduce() throws IOException {

		String inputStr = new String(Files.readAllBytes(Paths.get("access1")));
		mapReduceDriver.withInput(new LongWritable(), new Text(inputStr));
		
		mapReduceDriver.addOutput(new Text("Browser [Name=Chrome 54, version=54.0.2840.59, type=WEB_BROWSER]"), new IntWritable(3));
		mapReduceDriver.addOutput(new Text("Browser [Name=Chrome 54, version=54.0.2840.98, type=WEB_BROWSER]"), new IntWritable(6));
        mapReduceDriver.addOutput(new Text("Browser [Name=Firefox 49, version=49.0, type=WEB_BROWSER]"), new IntWritable(3));

		/*
		* Run the test.
		*/
		mapReduceDriver.runTest();
    }    


}
