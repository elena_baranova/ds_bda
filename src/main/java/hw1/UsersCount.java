/**
 * 
 */
package hw1;


import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

/**
 * @author cloudera
 *
 */
public class UsersCount {

	  public static class UsersCounterMapper 
	          extends Mapper<LongWritable, Text, AccessLogWritable, IntWritable>{

	    private final static IntWritable ONE = new IntWritable(1);
	    private AccessLogWritable UagentFromLog = new AccessLogWritable();
	 
	    private Text NameBrw = new Text();
	    private Text VersionBrw = new Text();
	    private Text TypeBrw = new Text();
	    private Text log_line = new Text();
	    
	    public static enum MAP_COUNTERS {
	        NOT_USER_AGENT_FIELD
	    }; 
	    
	    /**
	     * 
	     * @param key
	     * @param value
	     * @param context
	     * @throws IOException
	     * @throws InterruptedException
	     */
	    @Override
	    public void map(LongWritable key, Text value, Context context
	                    ) throws IOException, InterruptedException {
	      String line = value.toString();
	      StringTokenizer itr = new StringTokenizer(line, "\n");
	      
	      while (itr.hasMoreTokens()) {
	        log_line.set(itr.nextToken());
	        UserAgent userAgent = UserAgent.parseUserAgentString(log_line.toString());
	        Browser browser = userAgent.getBrowser();
	       
	        if (browser != Browser.UNKNOWN)
	         {
	            NameBrw.set(browser.getName());
	            VersionBrw.set(userAgent.getBrowserVersion().toString());
	            TypeBrw.set(browser.getBrowserType().toString());
	            
	            UagentFromLog.set(NameBrw, VersionBrw, TypeBrw);
	            context.write(UagentFromLog, ONE);
	          }
	         else {
	              context.getCounter(MAP_COUNTERS.NOT_USER_AGENT_FIELD).increment(1);
	              System.out.print("Counter value: ");
	              System.out.print(context.getCounter(MAP_COUNTERS.NOT_USER_AGENT_FIELD).getValue());
	              System.out.println("not user agent field");
	         }
	        }
	    }
	  }
	    
	  public static class IntSumReducer
	       extends Reducer<AccessLogWritable,IntWritable,Text,IntWritable> {
	    private final IntWritable result = new IntWritable();

	    @Override
	    public void reduce(AccessLogWritable key, Iterable<IntWritable> values,
	                       Context context
	                       ) throws IOException, InterruptedException {
	      int sum = 0;
	      for (IntWritable val : values) {
	        sum += val.get();
	      }
	      result.set(sum);
	      context.write(new Text(key.toString()), result);
	    }
	  }
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
	    Configuration conf = new Configuration();
	    Job job = Job.getInstance(conf, "Users count");
	    
	    job.setJarByClass(UsersCount.class);
	    job.setMapperClass(UsersCounterMapper.class);
	    job.setReducerClass(IntSumReducer.class);
	    
	    job.setOutputKeyClass(AccessLogWritable.class);
	    job.setOutputValueClass(IntWritable.class);
	    
	    job.setMapOutputKeyClass(AccessLogWritable.class);
	    job.setMapOutputValueClass(IntWritable.class);
	    

	    conf.set("mapreduce.map.output.compress", "true");
	    conf.set("mapreduce.map.output.compress.codec", "org.apache.hadoop.io.compress.SnappyCodec");
	    
//	    FileInputFormat.addInputPath(job, new Path(args[0]));
//	    FileOutputFormat.setOutputPath(job, new Path(args[1]));	    
	    FileInputFormat.addInputPath(job, new Path("hdfs://localhost/user/cloudera/input"));
	    FileOutputFormat.setOutputPath(job, new Path("hdfs://localhost/user/cloudera/output"));
	    
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
