/**
 * 
 */
package hw1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

/**
 * @author cloudera
 *
 */
public final class AccessLogWritable implements WritableComparable<AccessLogWritable>
{
	   private Text BrowserName; 
	   private Text BrowserVersion; 
	   private Text TypeOfBrowser;

	   /**
	    * Default Constructor - creation of new object
	    * @see AccessLogWritable#AccessLogWritable(Text, Text)
	    */
	   public AccessLogWritable() 
	   {
	        this.BrowserName = new Text();
	        this.BrowserVersion = new Text();
	        this.TypeOfBrowser = new Text();
	   }

	   /**
	    * Custom Constructor - creation of new object with specific values
	    * @param nameBr - name of using browser
	    * @param versionBr - version of using browser
	    * @param typeBr - type of using browser
	    * @see AccessLogWritable#AccessLogWritable()
	    */
	   public AccessLogWritable(Text nameBr, Text versionBr, Text typeBr) 
	   {
	        set(new Text(nameBr), new Text(versionBr), new Text(typeBr));
	   }

	   /**
	    * Setter method to set the values of AccessLogWritable object
	    * @param nameBr - name of using browser
	    * @param versionBr - version of using browser
	    * @param typeBr - type of using browser
	    */
	   public void set(Text nameBr, Text versionBr, Text typeBr) 
	   {
	        this.BrowserName = nameBr;
	        this.BrowserVersion = versionBr;
	        this.TypeOfBrowser = typeBr;
	   }

	   /**
	    * Method to get name of Browser from WebLog Record
	    * @return BrowserName
	    */
	   public Text getBrowserName()
	   {
	        return BrowserName; 
	   }
	   
	   /**
	    * Method to get version of Browser from WebLog Record
	    * @return BrowserVersion
	    */
	   public Text getBrowserVersion()
	   {
	        return BrowserVersion; 
	   }
	   
	   /**
	    * Method to get type of Browser from WebLog Record
	    * @return TypeOfBrowser
	    */
	   public Text getTypeOfBrowser()
	   {
	        return TypeOfBrowser; 
	   }
	   
	   
	   
	   /**
	    * Overriding default readFields method. 
	    * It de-serializes the byte stream data
	    * @param inputData
	    */
	   public void readFields(DataInput inputData) throws IOException 
	   {
	        BrowserName.readFields(inputData);
	        BrowserVersion.readFields(inputData);
	        TypeOfBrowser.readFields(inputData);
	   }

	   
	   /**
	    * Overriding default write method. 
	    * It serializes object data into byte stream data
	    * @param outData
	    */
	   public void write(DataOutput outData) throws IOException 
	   {
	        BrowserName.write(outData);
	        BrowserVersion.write(outData);
	        TypeOfBrowser.write(outData);
	   }
	   
	   @Override
	   /**
	    * Overriding default toString method. 
	    */
	   public String toString() {
	        return "Browser [Name=" + BrowserName + ", version=" + BrowserVersion + ", type=" + TypeOfBrowser + "]";
	   }
	  
	   /**
	    * Overriding default compareTo method. 
	    */
	   public int compareTo(AccessLogWritable obj) 
	   {
	     if (BrowserName.compareTo(obj.BrowserName)==0)
	     {
	        if (BrowserVersion.compareTo(obj.BrowserVersion)==0){
	            return (TypeOfBrowser.compareTo(obj.TypeOfBrowser));
	        }
	        else return (BrowserVersion.compareTo(obj.BrowserVersion));
	     }
	     else return (BrowserName.compareTo(obj.BrowserName));
	   }

	   /**
	    * Overriding default equals method. 
	    */
	   @Override
	   public boolean equals(Object obj) 
	   {
	     if (obj instanceof AccessLogWritable) 
	     {
	       AccessLogWritable other = (AccessLogWritable) obj;
	       return BrowserName.equals(other.BrowserVersion) && BrowserName.equals(other.BrowserVersion) && TypeOfBrowser.equals(other.TypeOfBrowser);
	     }
	     return false;
	   }

	   /**
	    * Overriding default hashCode method. 
	    */
	   @Override
	   public int hashCode()
	   {
	        return BrowserName.hashCode();
	   }
	   

}
